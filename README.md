# Unilist

Unilist, or ⋃nilist, is a simple and light page for copying Unicode characters.
It is inspired by [copypastecharacter.com](http://copypastecharacter.com/) but without advertising and with a free license.

## Contribute

If you want more characters, ask a pull request on the [`.charlist` text files](https://gitlab.com/raphaelbastide/unilist/tree/master/char). You can alternatively open an [issue](https://gitlab.com/raphaelbastide/unilist/issues) on this repository.

## In use

[unilist.raphaelbastide.com](http://unilist.raphaelbastide.com/)

## License

[GNU GPL](https://gnu.org/licenses/gpl.html)
