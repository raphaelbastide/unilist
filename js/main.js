
// Text boxes
$('nav button').click(function(e){
  e.preventDefault();
  var opener = $(this),
  oclass = opener.attr('class');
  pid = $('#' + oclass);
  $('body').toggleClass(oclass);
  pid.toggle();
});

const unireg = /([\0-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF])/g

$('.charlist').each(function(){
  var $this = $(this);
  var charz = $this.html();
  var newcharz = charz
    .match(unireg)
    .map(function(char) {
      return '<span data-clipboard-text="' + char + '">' + char + '</span>\n'
    })
    .join('');
	$this.html(newcharz);
})


// Clipboard
var btns = document.querySelectorAll('.charlist span');
var clipboard = new Clipboard(btns);
function anim(elm){
  elm.classList.remove('success');
}
clipboard.on('success', function(e) {
  var trigger = e.trigger;
  trigger.classList.add('success');
  setTimeout(function() {
    anim(trigger);
  }, 700)
});



$('.nav').change(function(){
  var opt = $(this).val();
  document.location.href= opt;
})
