<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>⋃nilist</title>
  	<meta name="Description" content="Unilist: Unicode Character list">
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <meta property="og:image" content="http://unilist.raphaelbastide.com/img/icon.png" />
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="font/stylesheet.css">
  </head>
  <body>
    <header>
      <h1><span>⋃</span>nilist</h1>
      <select class="nav">
        <option value="#classic"> Classic <span>✔</span></option>
        <option value="#arrows"> Arrows <span>⇝</span></option>
        <option value="#shapes"> Shapes <span>⧓</span></option>
        <option value="#symbols"> Symbols <span>☮</span></option>
        <option value="#ponctuation"> Ponctuation <span>¿</span></option>
        <option value="#math"> Math <span>∜</span></option>
        <option value="#numerals"> Numerals <span>❼</span></option>
        <option value="#currency"> Currency <span>₢</span></option>
        <option value="#emojis"> Emojis <span>🐱</span></option>
      </select>
    </header>

  <?php
  // Include Libs and Functions
  require_once "functions.php";
  // Recursively read the chosen directory for yaml content in .md
  $directory = "char/";
  $charcats = rglob($directory."{*.charlist}", GLOB_BRACE);
  $charcatNbr = 0;

  foreach ($charcats as $charcat) {
    // $charcat_name = basename($charcat, ".charlist");
    preg_match('/-(.*?)\./s', $charcat, $charcat_name);
    $charcatNbr ++;
    $cat_content = file_get_contents($charcat);
    $cat_content_nospace = str_replace('/^\s+|\n|\r|\s+$/m', '', $cat_content);
  ?>


  <section id="<?php echo $charcat_name[1]; ?>">
    <div class="charlist"><?php echo $cat_content_nospace; ?></div>
  </section>
  <?php } ?>
  <footer>
      By <a href="http://raphaelbastide.com/">Raphaël Bastide</a> ✎ <a href="https://gitlab.com/raphaelbastide/unilist">Source</a>
  </footer>
  <script src="js/jquery-1.9.1.min.js"></script>
  <script src="js/clipboard.min.js"></script>
  <script src="js/main.js"></script>
  </body>

</html>
